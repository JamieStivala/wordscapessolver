package wording.algorithim;

import java.util.ArrayList;
import java.util.Scanner;

public class LoadFile {
    private ArrayList<String> loadedFile;

    public LoadFile(){
        this("words.txt");
    }

    private LoadFile(String fileName){
        this.loadedFile = new ArrayList<>();
        try (Scanner scanner = new Scanner(getClass().getClassLoader().getResourceAsStream(fileName))){
            while (scanner.hasNextLine()) {
                this.loadedFile.add(scanner.nextLine());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public ArrayList<String> getFile(){
        return this.loadedFile;
    }
}
