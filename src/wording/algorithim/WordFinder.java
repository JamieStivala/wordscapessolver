package wording.algorithim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Pattern;

public class WordFinder {
    private String unformattedRegex;
    private String regex;

    private ArrayList<String> allCombinations;
    private HashMap<Character, Integer> duplicateAllowed;

    public WordFinder(String unformattedRegex, ArrayList<String> words) {
        this.unformattedRegex = unformattedRegex.toLowerCase();
        this.duplicateAllowed = characterCounter(this.unformattedRegex.toCharArray());

        regex = "[" + unformattedRegex + "]*";
        allCombinations = new ArrayList<>();

        for (String word : words) {
            if (isWordValid(word.toLowerCase())) allCombinations.add(word.toLowerCase());
        }

        //Remove duplicates
        HashSet<String> hashSet = new HashSet<>(allCombinations);
        allCombinations.clear();
        allCombinations.addAll(hashSet);
        //Remove duplicates
    }

    public ArrayList<String> byLength(int length){
        return byLength(this.allCombinations, length);
    }

    private ArrayList<String> byLength(ArrayList<String> currentPossibilities, int length){
        if(currentPossibilities == null || length > unformattedRegex.length()) return allCombinations;
        ArrayList<String> byLength = new ArrayList<>();
        for (String word: currentPossibilities) {
            if(word.length() == length){
                byLength.add(word);
            }
        }

        return byLength;
    }

    public ArrayList<String> byPosition(ArrayList<String> currentPossibilities, char[] knownCharacters){
        if(currentPossibilities == null) return null;
        if(knownCharacters == null || knownCharacters.length > unformattedRegex.length()) return currentPossibilities;

        //Calculate the amount of blank characters
        int blankChar = 0;
        for (char c: knownCharacters) {
            if(c == Character.MIN_VALUE) blankChar++;
        }
        //Calculate the amount of blank characters


        ArrayList<String> newPossibilities = new ArrayList<>();
        HashMap <String, Integer> stringIntegerHashMap = new HashMap<>();
        for (String word: currentPossibilities) {
            char splitWord[] = word.toCharArray();
            for (int i = 0; i != splitWord.length ; i++) {
                if(splitWord[i] == knownCharacters[i]){
                    stringIntegerHashMap.computeIfPresent(word, (k, v) -> ++v);
                    stringIntegerHashMap.putIfAbsent(word, 0);
                }
            }
        }

        for (String word: stringIntegerHashMap.keySet()) {
            if(stringIntegerHashMap.get(word) + 1 == knownCharacters.length - blankChar) newPossibilities.add(word);
        }

        return newPossibilities;
    }


    private boolean isWordValid(String word){
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        if(word.length() < 3 || word.length() > unformattedRegex.length() || !p.matcher(word).matches()) return false;
        HashMap<Character, Integer> duplicateCharacters = characterCounter(word.toCharArray());

        for (char x: duplicateCharacters.keySet()) {
            if(this.duplicateAllowed.get(x) < duplicateCharacters.get(x)) return false;
        }

        return true;
    }

    private HashMap<Character, Integer> characterCounter(char chars[]){
        HashMap<Character, Integer> characterIntegerHashMap = new HashMap<>();

        for(int i = 0; i != chars.length; i++){
            characterIntegerHashMap.computeIfPresent(chars[i], (k, v) -> ++v);
            characterIntegerHashMap.putIfAbsent(chars[i], 0);
        }
        return characterIntegerHashMap;
    }
}
