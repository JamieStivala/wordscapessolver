import wording.algorithim.LoadFile;
import wording.algorithim.WordFinder;

import javax.swing.*;
import java.util.ArrayList;

public class Runner {
    public static void main(String args []){
        String letters = JOptionPane.showInputDialog("Please enter all the letters");
        WordFinder wordFinder = new WordFinder(letters, new LoadFile().getFile());

        String options[] = new String[]{"Length", "Letters", "Exit"};
        int choice = 20;

        while(choice != 2) {
            choice = JOptionPane.showOptionDialog(null, "Please choose an option, ", "Select option", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, "Please choose");

            if(choice == 0){
                int length = Integer.parseInt(JOptionPane.showInputDialog("Please enter the length"));
                String known = JOptionPane.showInputDialog("Enter known words");

                ArrayList<String> words = wordFinder.byLength(length);

                char [] knownSplit = known.toLowerCase().toCharArray();
                int counterUnder = 0;
                for (int i = 0; i != knownSplit.length ; i++) {
                    if(knownSplit[i] == '_'){
                        counterUnder++;
                        knownSplit[i] = Character.MIN_VALUE;
                    }
                }


                if(counterUnder < knownSplit.length){
                    words = wordFinder.byPosition(words, knownSplit);
                }

                StringBuilder stringBuffer = new StringBuilder();

                for (String b:  words) {
                    stringBuffer.append(b);
                    stringBuffer.append("\n");
                }


                JOptionPane.showMessageDialog(null, stringBuffer.toString());
            }

        }


    }
}
